package com.example.afrijal.cekktp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout linearLayout;
    EditText mNik;
    Button btn;
    TextView txtNik, txtnama, txtkelamin, txtkelurahan, txtkecamatan, txtkabupaten, txtprovinsi, nik, nama, kelamin, kelurahan, kecamatan, kabupaten, provinsi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //panggil id
        linearLayout = (LinearLayout) findViewById(R.id.ln1);

        mNik = (EditText) findViewById(R.id.mNik);

        btn = (Button) findViewById(R.id.btn);

        txtNik = (TextView) findViewById(R.id.txtNik);
        txtnama = (TextView) findViewById(R.id.txtnama);
        txtkelamin = (TextView) findViewById(R.id.txtkelamin);
        txtkelurahan = (TextView) findViewById(R.id.txtkelurahan);
        txtkecamatan = (TextView) findViewById(R.id.txtkecamatan);
        txtkabupaten = (TextView) findViewById(R.id.txtkabupaten);
        txtprovinsi = (TextView) findViewById(R.id.txtkabupaten);

        nik = (TextView) findViewById(R.id.nik);
        nama = (TextView) findViewById(R.id.nama);
        kelamin = (TextView) findViewById(R.id.kelamin);
        kelurahan = (TextView) findViewById(R.id.kelurahan);
        kecamatan = (TextView) findViewById(R.id.kecamatan);
        kabupaten = (TextView) findViewById(R.id.kabupaten);
        provinsi = (TextView) findViewById(R.id.provinsi);

        btn.setOnClickListener(this);


    }


    private void getDataWarga(String mNik) {
        String url = "http://ibacor.com/api/ktp?nik=" + mNik;

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("DATA", response.toString());
                try {
                    JSONObject object = new JSONObject(response.toString());

                    JSONObject data = object.getJSONObject("data");

                    String dnik = data.getString("nik");
                    String dnama = data.getString("nama");
                    String dkelamin = data.getString("kelamin");
                    String dkelurahan = data.getString("kelurahan");
                    String dkecamatan = data.getString("kecamatan");
                    String dkabupaten = data.getString("kabupaten_kota");
                    String dprovinsi = data.getString("provinsi");

                    nik.setText(dnik);
                    nama.setText(dnama);
                    kelamin.setText(dkelamin);
                    kelurahan.setText(dkelurahan);
                    kecamatan.setText(dkecamatan);
                    kabupaten.setText(dkabupaten);
                    provinsi.setText(dprovinsi);

                    String mnik = "NIK";
                    String mnama = "Nama";
                    String mkelamin = "Jenis Kelamin";
                    String mkelurahan = "Kelurahan";
                    String mkecamatan = "Kecamatan";
                    String mkabupaten = "Kabupaten / Kora";
                    String mprovinsi = "Provinsi";

                    txtNik.setText(mnik);
                    txtnama.setText(mnama);
                    txtkelamin.setText(mkelamin);
                    txtkelurahan.setText(mkelurahan);
                    txtkecamatan.setText(mkecamatan);
                    txtkabupaten.setText(mkabupaten);
                    txtprovinsi.setText(mprovinsi);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("DATA", error.toString());
                Toast.makeText(getApplicationContext(), "No Connectivity Available", Toast.LENGTH_LONG).show();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(request);
    }

    public void onClick(View view) {

        if (view == btn) {


            if (mNik.getText().toString().matches("")) {
                Toast.makeText(getApplicationContext(), "Masukkan NIK Anda", Toast.LENGTH_LONG).show();

            } else {
                String nik = mNik.getText().toString();

                try {

                    getDataWarga(nik);
                    mNik.setText("");


                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Error Connection", Toast.LENGTH_SHORT).show();
                }
            }
        }


    }
}